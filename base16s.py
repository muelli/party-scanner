#!/usr/bin/env python3
# base16s: create base16 encoded strings
# Copyright (C) 2019 Tobias Mueller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import base64
import sys

for line in sys.stdin.buffer:
    line = line.strip()
    if line:
        print(base64.b16encode(line).decode('ascii'))
