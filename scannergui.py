#!/usr/bin/env python3
# scanner: GUI to run a scanner for data
# Copyright (C) 2019 Tobias Mueller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import datetime
import fileinput
import logging
import sys
from tkinter import Tk, font, filedialog, Label, Button, Entry, StringVar, DISABLED, NORMAL, END, W, E
import tkinter as tk

log = logging.getLogger(__name__)

def set_text(entry, text):
    entry.delete(0,END)
    entry.insert(0,text)
    return

def set_label_text(label, text):
    label.config(text=text)
    return

def resize(label, label_font, event):
        height = label.winfo_height()
        width = label.winfo_width()
        height = height // 2
        log.info('height %s' % height)
        log.info('width %s' % width)
        if height < 10 or width < 200:
            height = 10
        elif width < 400 and height > 20:
            height = 20
        elif width < 600 and height > 30:
            height = 30
        else:
            height = 40
        log.info('height %s' % height)

        label_font['size'] = height
        log.info("Actual: %s", label_font.actual())





class App(tk.Frame):
    def __init__(self, master=None):
            super().__init__(master)
            self.pack()

class FullScreenApp:
    def __init__(self, master, matches, **kwargs):
        self.master=master
        pad=3
        self._geom='200x200+0+0'
        master.geometry("{0}x{1}+0+0".format(
            master.winfo_screenwidth()-pad, master.winfo_screenheight()-pad))
        master.bind('<Escape>',self.toggle_geom)

        self.matched_codes = []
        self.matches = matches
        self.create_widgets()

    def toggle_geom(self,event):
        geom=self.master.winfo_geometry()
        print(geom,self._geom)
        self.master.geometry(self._geom)
        self._geom=geom

    def create_widgets(self):
        #w.title = "Code Scanner"
        master = self.master
        tk.Label(master, text="Code:").pack()
        entry = tk.Entry(master)
        entry.focus_set()
        entry.pack()
        result_font = font.Font(self.master,
            family='Arial', size=12, weight='bold')
        res = tk.Label(master, text="Results", font=result_font)
        res['background'] = "green"
        res.pack(fill=tk.BOTH, expand=1)
        res.bind("<Configure>", lambda e: resize(res, result_font, e))
        entry.bind("<Return>", lambda x: self.evaluate(entry, x, res))

        self.master.winfo_toplevel().title("Scanner App")

    def event_trigger(self, when, event, line):
        log.info("Triggered for: %s %s %s", when, event, line)

    def evaluate(self, entry, e, result_widget):
            log.info("Evaluate: %s", e)
            log.info("Entry: %r", entry.get())
            now = datetime.datetime.now()
            line = entry.get().strip()
            if not line in self.matches:
                event = "notmatched"
                log.info("Line does not match: %r", line)
                result_widget["background"] = "red"
                set_label_text(result_widget, line)
            else:
                log.info("Found match!")
                if line in self.matched_codes:
                    event = "alreadymatched"
                    log.warning("Code has already matched!")
                    result_widget["background"] = "orange"
                    set_label_text(result_widget, line)
                else:
                    event = "matched"
                    self.matched_codes.append(line)
                    result_widget["background"] = "green"
                    set_label_text(result_widget, line)

            set_text(entry, "")
            self.event_trigger(now, event, line)



def main():
    parser = argparse.ArgumentParser(description=""
        "Scan barcodes from a list")
    parser.add_argument("input-file", nargs='?',
        help="The file with the strings to match to use",
        default=None,
    )

    #logging.basicConfig(level=logging.INFO)
    logging.basicConfig(level=logging.DEBUG)

    args = parser.parse_args()
    log.warning("args: %r", args)

    root = tk.Tk()
    input_file = vars(args)["input-file"]

    output_filename = datetime.datetime.now().strftime("scanner-output-%Y-%m-%d--%H-%m-%S.txt")
    output_file = open(output_filename, 'x')

    log.info("Input file: %r", input_file)

    def load_matches(input_file):
        if input_file is None:
            input_file = filedialog.askopenfilename(initialdir = ".",
                    title = "Eingabedatei",
                    filetypes = (("text files","*.txt"),) #("all files","*.*"))
                )
            root.update()

        matches = { l.strip() for l in open(input_file, "r") if l.strip() }
        log.debug("Loaded matches: %s", matches)
        return matches


    if False: # or True:
        w = tk.Tk()
        w.title = "Code Scanner"
        tk.Label(w, text="Code:").pack()
        entry = tk.Entry(w)
        entry.pack()
        res = tk.Label(w, text="Results")
        res['background'] = "green"
        res.pack(fill=tk.BOTH, expand=1)
        entry.bind("<Return>", lambda x: evaluate(entry, x, res))
        w.mainloop()

    if False or True:
        root.update()
        matches = load_matches(input_file)
        app = FullScreenApp(root, matches)
        root.update()
        app.master.title = "Scanner App"
        #app.master.maxsize(1000, 400)

        def event_trigger(when, event, line):
            log.info("Outer trigger: %s, %s, %s", when, event, line)
            l = ",".join((when.isoformat(), event, line))
            output_file.write(l+"\r\n")
            output_file.flush()

        app.event_trigger = event_trigger
        root.mainloop()

    #matcher(contents, sys.stdin, output_file)


def matcher(matches, input_file, output_file,
        matched_cb=None, nomatch_cb=None, alreadymatched_cb=None):
        
    matched_codes = []
    with fileinput.input("-") as f:
        for line in (l.strip() for l in f):
            log.info("Read line: %s", line)
            if not line in matches:
                log.info("Line does not match: %r", line)
            else:
                log.info("Found match!")
                if line in matched_codes:
                    log.warning("Code has already matched!")
                else:
                    matched_codes.append(line)

if __name__ == "__main__":
    main()
