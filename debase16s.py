#!/usr/bin/env python3
# debase16s: decode base16 encoded strings
# Copyright (C) 2019 Tobias Mueller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import base64
import sys

filter_commas = False
filter_notmatched = True

for lines in sys.stdin.buffer:
    for (i, line) in enumerate(lines.splitlines()):
        line = line.strip()
        if line:
            if filter_notmatched:
                if b'notmatched' in line:
                    print ("filtering notmatched: %r" % line, file=sys.stderr)
                    continue

            # comma in lines
            if filter_commas:
                line = line.split(b',', 2)[2]

            print ("Decoding line %d: %r" % (i, line), file=sys.stderr)
            decoded = base64.b16decode(line).decode('latin-1').strip()
            print (decoded)
