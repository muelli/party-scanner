#!/usr/bin/env python3
# runqr: launch "qr" processes to encode certain data
# Copyright (C) 2019 Tobias Mueller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import base64
import subprocess
import sys

for i, name in enumerate(open(sys.argv[1], 'rb')):
    name16 = name.strip()
    cmd = ["qr",
        "--error-correction=L",
        "--optimize=2",
        "--factory=pil",
        name16,
    ]
    target_fname = "%05d-%s.png" % (i+1, name16.decode('ascii')[:30]])
    subprocess.check_call(cmd, stdout=open(target_fname, 'w'))
